![NEAlertKit_banner.png](https://bitbucket.org/repo/kL8bGx/images/211663643-NEAlertKit_banner.png)

# NEAlertKit

###*Any iOS, any device, all the { UIAlertView / UIActionSheet / UIAlertController } stuff wrapped up into 1 Easy-to-Use Framework with inline code execution blocks^{...}*
  
  
## INTRO
A powerful framework built around a set of convenience methods to implement the correct UIAlertView/UIActionSheet/UIAlertController based on the run-time iOS version AND provide inline SelectionBlock^{} execution.  You no longer need delegates or code in multiple places.

Access is provided directly to UIViewController Class as an Extension (Highly Recommended!) OR you can access through NEAlertController class directly.

**Note:** This framework uses the real (UIAlertController, UIAlertView, and UIActionSheet) native classes; there is no suite of custom classes trying to imitate the native Apple implementations.  You get all the same look, feel, and methods.

  

## MIN REQUIREMENTS:
____
* Xcode 6.3 or Higher
* Build for iOS 5.0 or Higher

## SUPPORTED LANGUAGES:
____
* Objective-C
* Swift  (All methods provide Nullability references for Swift Interpolation)

 
## INSTALLATION:  (Drag & Drop)
____
The installation is extremely easy and simple; its drag and drop.  However, sometimes it can be easy for developers fail to provide warnings on *gotchas* or supply other *tacit knowledge* that can make even the most basic installation a pain.  In my installation steps below, I identify *each and every step* with extreme clarity so there are no misunderstandings.  I would rather you spend your time using the framework, not fighting with the install.
  

1.  **DOWNLOAD** NEAlertKitApp sample project.
2.  **BUILD** sample application to gain experience and ensure framework is working.
3.  **OPEN** your Xcode Project
4.  In the Project Navigator window, **SELECT** your project file.
5.  **SELECT** your application target under *'TARGETS'*
6.  **SELECT** 'General' tab
7.  **SCROLL** down until you see *'Embedded Binaries'*
8.  (IF collapsed) **SELECT** the *'Embedded Binaries'* title to **EXPAND** the container. 
9.  **DRAG** the *'NEAlertKit.framework'* **INTO** the *'Embedded Binaries'* list area.
10. **ENSURE** *'Copy items if needed'* is selected.
11. **ENSURE** *'Create folder references'* is selected.
12. **SELECT** *'Finish'* button.
12. **BUILD** project.

![drag_framework_into_binaries.png](https://bitbucket.org/repo/kL8bGx/images/171362608-drag_framework_into_binaries.png)

## DOCUMENTATION:
____
The code's public interfaces are well-documented with extensive explanation and even sometimes examples.
  
All methods are documented for Xcode intellisense.
 

![code_intellisense.png](https://bitbucket.org/repo/kL8bGx/images/3806264371-code_intellisense.png)

![documented_quick_help.png](https://bitbucket.org/repo/kL8bGx/images/764922825-documented_quick_help.png)




## USAGE: (3 Easy Steps)
____

1. **IMPORT** ```#import <NEAlertKit/NEAlertKit.h>``` framework into any class's implementation (.m) file that you want to use the NEAlertKit. *(Don't worry about importing any delegate protocols, unless you need it-see DELEGATION section for more).*  {If you know your implementation will always run on iOS 7.0 or greater then you can import the NEAlertKit Module using ```@import NEAlertKit;```} both are compatible.
 
2. **CALL** desired alert, either an 'Alert' or 'ActionSheet'.  There are really only two methods (```showAlertWithTitle:``` or ```showActionSheetWithSender:```), everything else is syntactic sugar.  However, you can call the NEAlertController class (as in ```[NEAlertController showAlert...```) if you need to for more access.
 
3. **USE** the 'selectionBlock' to execute in-line code.

|  Desired Alert  	| From UIViewController (category) 	|
|:---------------:	|:--------------------------------:	|
| Simple 'Ok' Alert 	|  ``` [self showOkAlertWithTitle: ```           |
|      Alert      	|  ``` [self showAlertWithTitle:    ```          |
|  Alert with Image     |  ``` [self showImageAlertWithTitle:    ```          |
|   ActionSheet   	|  ``` [self showActionSheetWithSender:   ```    |


   
   

# ALERT EXAMPLES
========
========
### 1. Simple 'Ok' Alert (Simplest Example) ###

Designed to address the 90% case-scenario when you just need to let the user know something.

```
#import "SomeUIViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@implementation SomeUIViewController : UIViewController

- (void)showOkAlert {

    [self showOkAlertWithTitle:@"Title"
                       message:@"Message"
                selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {

                    NSLog(@"User selection made, perform actions in here...");

    }];
}

@end 
```

![simple_alert_1.png](https://bitbucket.org/repo/kL8bGx/images/3800650807-simple_alert_1.png)


### 2.  Default Alert Style with a lot of buttons and destructive button: ###
```
#import "SomeUIViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@implementation SomeUIViewController : UIViewController

- (IBAction)showAlert:(id)sender {
    
    [self showAlertWithTitle:@"Alert Title"
                     message:@"Lots of buttons..."
                       style:NEAlertStyle_Default
           cancelButtonTitle:@"Cancel"
      destructiveButtonTitle:@"Destructive"
           otherButtonTitles:@[@"a", @"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i"]
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  // Your code here...
                  
              }];
    
}

@end
```
![lots_of_buttons.png](https://bitbucket.org/repo/kL8bGx/images/1969821152-lots_of_buttons.png)

### 3.  Secure Text Input ###
```
#import "SomeUIViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@implementation SomeUIViewController : UIViewController

- (IBAction)showAlert:(id)sender {
    
    [self showAlertWithTitle:@"Title"
                     message:@"Secure Text Input"
                       style:NEAlertStyle_SecureTextInput
           cancelButtonTitle:@"Submit"
      destructiveButtonTitle:nil
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  NSLog(@"Text Input: %@", [alertController.textFields.firstObject text]);
                  
                  
              }];
    
}

@end
```

![secure_text_Input.png](https://bitbucket.org/repo/kL8bGx/images/3325018890-secure_text_Input.png)

### 4.  Plain Text Input ###
```
#import "SomeUIViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@implementation SomeUIViewController : UIViewController

- (IBAction)showAlert:(id)sender {
    
    [self showAlertWithTitle:@"Title"
                     message:@"Secure Text Input"
                       style:NEAlertStyle_PlainTextInput
           cancelButtonTitle:@"Submit"
      destructiveButtonTitle:nil
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  NSLog(@"Text Input: %@", [alertController.textFields.firstObject text]);
                  
                  
              }];
    
}

@end
```
![plain_text_input.png](https://bitbucket.org/repo/kL8bGx/images/449828552-plain_text_input.png)

### 5. Login & Password
You don't have to manually try to build these in UIAlertController, NEAlertKit handles all the setup and provides delegate access (if needed)

```
#import "YourViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@interface YourViewController ()

@end

@implementation YourViewController

- (IBAction)showAlert:(id)sender {
    
    [self showAlertWithTitle:@"Title"
                     message:@"Login & Password"
                       style:NEAlertStyle_LoginAndPasswordInput
           cancelButtonTitle:@"Submit"
      destructiveButtonTitle:nil
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  NSLog(@"Login: %@", [alertController.textFields[0] text]);
                  
                  NSLog(@"Password: %@", [alertController.textFields[1] text]);
                  
                  //Use the information...

              }];
    
}

@end
```
![login_password.png](https://bitbucket.org/repo/kL8bGx/images/1856746728-login_password.png)


### 6.  Alert with Image
**NOTE**:  In this example you can see how either  ```NEAlertContrller``` or ```self``` can access the NEAlertKit methods; they are interchangeable.  Both implementations call the exact same method.
```
#import "YourViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@interface YourViewController ()

@end

@implementation YourViewController

- (IBAction)showAlert1:(id)sender {
    
    UIImage *img = [UIImage imageNamed:@"code_icon"];
    
    
    [self showImageAlertInViewController:self
                               withTitle:@"Title"
                                 message:@"message"
                              sizedImage:img
                       cancelButtonTitle:@"Ok"
                  destructiveButtonTitle:nil
                       otherButtonTitles:nil
                          selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                           
                                  NSLog(@"%@", buttonTitle);
                            }];
    
}

- (IBAction)showAlert2:(id)sender {
    
    UIImage *img = [UIImage imageNamed:@"code_icon"];
    
    
    [NEAlertController showImageAlertInViewController:self
                                            withTitle:@"Title"
                                              message:@"message"
                                           sizedImage:img
                                    cancelButtonTitle:@"Ok"
                               destructiveButtonTitle:nil
                                    otherButtonTitles:@[@"one", @"two"]
                                       selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                           
                                             NSLog(@"%@", buttonTitle);
                                       }];
    
}

@end
```
| Alert1 with just 'Ok' Button 	| Alert2 With Multiple Button Options 	|
|:---------------:	|:----------------------------------:	|
|       ![image_example1.png](https://bitbucket.org/repo/kL8bGx/images/2319072590-image_example1.png)       	|                 ![image_example2.png](https://bitbucket.org/repo/kL8bGx/images/3574192358-image_example2.png)                	|



### 7.  ADVANCED: Extreme case of Nested Alerts, (Highlighting Dynamic Businesses Logic): ###
In many cases, the User's input can lead to complex logic trees to cover all cases.  When the alerts are nested, covering all scenarios is much easier to walk down each possible outcome.  This is extreme case to show the power of leveraging nested alerts; however, it may be applicable for other cases such as gathering key sequential information from a user *(like a setup wizard)* to correctly began a process.

```
#import "SomeUIViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@implementation SomeUIViewController : UIViewController


- (IBAction)showAlert:(id)sender {
    
    //  Extremely Easy to nest Business Rules or User Input Logic all inline!!
    //  In this example, there is a simple NSString 'selections' keeping track of the selections just for the demo.
    //  However, in a real app, each selection can offer a ```switch()``` or ```if()else()``` logic tree to make the correct logic decisions based on your application needs.  
    
    __block NSString *selections = @"";
    
    
    //Alert #1:
    [self showAlertWithTitle:@"First Alert 1"
                     message:@"Nested Alerts (Implement logic all inline)"
                       style:NEAlertStyle_Default
           cancelButtonTitle:nil
      destructiveButtonTitle:nil
           otherButtonTitles:@[@"A", @"B", @"C"]
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  selections = [selections stringByAppendingFormat:@"%@",buttonTitle];
                  
                  
                  
                  //Nested Alert #2:
                  [self showAlertWithTitle:@"Nested Alert 2"
                                   message:[NSString stringWithFormat:@"You selected: %@", buttonTitle]
                                     style:NEAlertStyle_Default
                         cancelButtonTitle:nil
                    destructiveButtonTitle:nil
                         otherButtonTitles:@[@"D", @"E", @"F"]
                            selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                
                                selections = [selections stringByAppendingFormat:@"-%@",buttonTitle];
                                
                                
                                
                                //Nested Alert #3:
                                [self showAlertWithTitle:@"Nested Alert 3"
                                                 message:[NSString stringWithFormat:@"You selected: %@", buttonTitle]
                                                   style:NEAlertStyle_Default
                                       cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                       otherButtonTitles:@[@"H", @"I", @"J"]
                                          selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                              
                                              selections = [selections stringByAppendingFormat:@"-%@",buttonTitle];
                                              
                                              
                                              
                                              //Nested Alert #4:
                                              [self showOkAlertWithTitle:@"Nested Alert 4"
                                                                 message:[NSString stringWithFormat:@"Your Nested Selections so far: %@", selections]
                                                          selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                                  
                                                              //Final Log:
                                                              NSLog(@"%@", [NSString stringWithFormat:@"Your Nested Selections: %@", [selections stringByAppendingFormat:@"-%@",buttonTitle]]);
                                                              
                                                          }];
                                               
                                        
                                          }];
                                
                                
                            }];
                  
                  
              }];

    
}

@end
```


 
### 8.  ADVANCED: Custom UITextFieldDelegate (Intercepting TextField Input) ###


1.  **ADD** the ```<UITextFieldDelegate>``` to your class's implementation (.m) interface 

2.  **UTILIZE** any of the UITextFieldDelegate methods, as shown below.

3.  **TEST** textField's.tag against the NEAlertController ```typedef NS_ENUM(NSInteger, NEAlertTextFieldTag) {..}```


| NEAlertTextFieldTag               	| Example                                                       	|
|-----------------------------------	|---------------------------------------------------------------	|
| NEAlert_SecureInput_TextField_Tag 	| if (textField.tag == NEAlert_SecureInput_TextField_Tag) {...} 	|
| NEAlert_PlainInput_TextField_Tag  	| if (textField.tag == NEAlert_PlainInput_TextField_Tag) {...}  	|
| NEAlert_Login_TextField_Tag       	| if (textField.tag == NEAlert_Login_TextField_Tag) {...}       	|
| NEAlert_Password_TextField_Tag    	| if (textField.tag == NEAlert_Password_TextField_Tag) {...}    	|

   
  
```
#import "YourViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@interface YourViewController () <UITextFieldDelegate>

@end

@implementation YourViewController


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    

    if (textField.tag == NEAlert_Login_TextField_Tag) {
        
        //Login...
        
    }else if (textField.tag == NEAlert_Password_TextField_Tag) {
     
        //Password...
    }
    
    NSLog(@"%@ || %@ || %@", string, textField.text, textField);
    
    
    return YES;
}

@end

```

  
  

# ActionSheet Examples
========
========


###  1. Just like the Alerts, ActionSheets are one liners you can call directly from the UIViewController

```
#import "YourViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@interface YourViewController ()

@end

@implementation YourViewController

- (IBAction)showActionSheet:(id)sender {
    
    [self showActionSheetWithSender:sender
                              title:@"Title"
                            message:@"Message"
                  cancelButtonTitle:@"cancel"
             destructiveButtonTitle:@"destructive"
                  otherButtonTitles:@[@"a",@"b",@"c"]
                     selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                         
                         
                         if(buttonIndex == alertController.cancelButtonIndex) {
                             
                             // Handle Cancel...
                         
                         } else if(buttonIndex == alertController.destructiveButtonIndex) {
                             
                             // Handle Destruction...
                         
                         } else if([buttonTitle isEqualToString:@"a"]) {
                             
                             // Handle A Case...
                             
                         }else if([buttonTitle isEqualToString:@"b"]) {
                             
                             //Handle B Case...
                             
                         }else if([buttonTitle isEqualToString:@"c"]) {
                         
                             //Handle C Case...
                         }
                         
                     }];
}

@end
```

###  (On iPhones)  ActionSheets are preseted from bottom, just like normal UIActionSheet


![iphone_action_sheet.png](https://bitbucket.org/repo/kL8bGx/images/373481554-iphone_action_sheet.png)

###  (On iPads) ActionSheets are Presented in UIPopoverViewController anchored off the selection button.
**Note:** the 'Cancel' button is not displayed, this is normal Apple behavior, the user can selected anywhere on screen to 'cancel' their selection.  However, your application's logic does not change!  You still get the correct dismiss *buttonIndex* and *buttonTitle* back regardless of user's selection.


| Anchored off View UIButton 	| Anchored off Toolbar UIBarButonItem 	| Anchored off Navbar UIBarButonItem 	|
|:--------------------------:	|:-----------------------------------:	|:----------------------------------:	|
|             ![action_sheets_anchored_off_buttons.png](https://bitbucket.org/repo/kL8bGx/images/3562573704-action_sheets_anchored_off_buttons.png)            	|                 ![action_sheet_anchored_1.png](https://bitbucket.org/repo/kL8bGx/images/3650017187-action_sheet_anchored_1.png)                 	|                ![action_sheet_anchored_2.png](https://bitbucket.org/repo/kL8bGx/images/4285361332-action_sheet_anchored_2.png)                	|




###  2.  ADVANCED:  UIPopoverPresentationController Customization (on ActionSheets on iPads) use ```popoverPresentationControllerBlock^{}```

*  For 99% of most cases, use the UIViewController category methods and you will not need to worry about the UIPopoverViewController requirements for iPad, they are handled under-the-hood for you.
 
*  However, for those cases where you want direct access, then use the NEAlertController class directly as shown below.
 
*  If you FAIL to PROVIDE the UIPopoverPresentationController with the required popover.sourceView and popover.sourceRect), then you will receive an ASSERT from the UIPopoverPresentationController.
 

```
#import "YourViewController.h"
#import <NEAlertKit/NEAlertKit.h>

@interface YourViewController ()

@end

@implementation YourViewController

- (IBAction)showActionSheetWithCustomPopOverController:(id)sender {

    [NEAlertController showActionSheetInController:self
                                         withTitle:@"Title"
                                           message:@"Message"
                                 cancelButtonTitle:@"Cancel"
                            destructiveButtonTitle:nil
                                 otherButtonTitles:@[@"a"]
                presentationPopoverControllerBlock:^(NEPopoverPresentationController * _Nonnull popover) {
                    
                    
                    //Case A: Assuming the  sender is Basic UIButton:
                    popover.sourceView = self.view;
                    popover.sourceRect = [(UIView *)sender frame];
                    
                    // OR...
                    
                    //Case B: Assuming the sender is UIBarButtonItem
                    popover.barButtonItem = (UIBarButtonItem *)sender;
                    UIView *view = [sender valueForKey:@"view"];
                    popover.sourceView = view;
                    popover.sourceRect = view.frame;
                    
                    // OR....
                    
                    //Case A|B: Alternative Defensive Method:
                    if ([sender isKindOfClass:[UIBarButtonItem class]]) {
                        popover.barButtonItem = (UIBarButtonItem *)sender;
                        UIView *view = [sender valueForKey:@"view"];
                        if(view != nil) {
                            popover.sourceRect = view.frame;
                            popover.sourceView = view;
                        }
                    }else if ([sender respondsToSelector:@selector(frame)]) {
                        popover.sourceView = self.view;
                        popover.sourceRect = [(UIView *)sender frame];
                    }
                    

                }
                                    selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                        

                                        if(buttonIndex == alertController.cancelButtonIndex) {
                                            
                                            // Handle Cancel...
                                            
                                        } else if([buttonTitle isEqualToString:@"a"]) {
                                            
                                            // Handle A Case...
                                            
                                        }
                                    }];
    
}

@end
```
 
**Note:**  For the 99% of cases, this is all handled for you, however, read more about below if you need extra control.
 
**See Also:**  ```UIPopoverPresentationController : UIPresentationController <UIPopoverPresentationControllerDelegate> delegate```



# ADDITIONAL INFORMATION   
========
========

### TYPES OF ALERTS (Alerts and ActionSheets)

|       NEAlertControllerStyle       	|                                         Description                                        	|
|:----------------------------------:	|:------------------------------------------------------------------------------------------:	|
| NEAlertControllerStyle_Alert       	| Alert-Style presented NEAlertController  (Good old fashion UIAlertView style alert)        	|
| NEAlertControllerStyle_ActionSheet 	| ActionSheet-Style presented NEAlertController (Good old fashion UIActionSheet style alert) 	|



### ALERT STYLES
Alerts offer a few 'preconfigured' options.


|            NEAlertStyle            	|                                      Description                                     	|
|:----------------------------------:	|:------------------------------------------------------------------------------------:	|
| NEAlertStyle_Default               	| An alert that presents a standard title, message; original UIAlertView-styled alert. 	|
| NEAlertStyle_SecureTextInput       	| An alert that presents a title, message, and single secure text input.               	|
| NEAlertStyle_PlainTextInput        	| An alert that presents a title, message, and single text input.                      	|
| NEAlertStyle_LoginAndPasswordInput 	| An alert that presents a title, message, and both login & password text fields.      	|



## ASSERTIONS:

| Assert 	|                                                                                                   Assertion Description                                                                                                  	|                                                                     Console Output                                                                     	|
|:------:	|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:	|:------------------------------------------------------------------------------------------------------------------------------------------------------:	|
|    1   	| Failure to provide at least one dismiss button will raise an exception.,Your user must have a means to dismiss the alert.                                                                                                	| ```*** NEAlertController Assert: Invalid Alert Request. (No Valid Buttons to dismiss Alert displayed to user.) ***```                                	|
|    2   	| Attempting to use *showImageAlertInViewController:* on other than iPad/iPhone/iPod device will raise an exception. (*Note*: Mac, AppleTV, CarPlay are not currently supported for the image alert messages at this time) 	| ```*** NEAlertController Assert: Invalid Alert Request.,(Unsupported Device : NEAlertController showSizedImage: not supported on this device.) ***``` 	|

  
  
## DELEGATION:  (Not Needed !!)
____
*unless you want to...*
  
- You do not have to include either <UIAlertViewDelegate, UIActionSheetDelegate> Protocols.
- If you do, your delegate methods will be called *AFTER* the internal NEAlertKit.Framework methods fire.  (However, not sure you will need them after you get into use the framework)
- *UITextFields*  (The internal UITextFields created will honor your UIViewController as delegate.  If you provide the <UITextfieldDelegate> protocol and implement the methods, you can intercept your textfield operations/inputs as normal.)  NEAlertKit conforms to ```@protocol UITextFieldDelegate```



## NSLocalization
____
NEAlertKit will honor the following NSLocalizedString keys, if you are using ```NSLocalizedString(key,comment)``` and have provided a strings binary.  Console logs are in English.  No other strings are used with the framework.  What you enter is what you get out.
  
| Keys                                     	| Usage                   	|
|------------------------------------------	|-------------------------	|
| NSLocalizedString(@"Ok", nil)            	| Dismiss Button          	|
| NSLocalizedString(@"Secure Input", nil)  	| UITextField Placeholder 	|
| NSLocalizedString(@"Provide Input", nil) 	| UITextField Placeholder 	|
| NSLocalizedString(@"Login", nil)         	| UITextField Placeholder 	|
| NSLocalizedString(@"Password", nil)      	| UITextField Placeholder 	|


  
## EXTRAs:
____
Beyond all the normal UIAlertView, UIActionSheet, and UIAlertController methods and features, the following are additional features squeezed in for convenience and consistency:

1.  ActionSheets honor 'messages', even < iOS 8.0  (When running on older UIActionSheets the message is automatically added onto the title with a new line '\n' and shown)

2.  Image support for Alerts is provided

3.  Destructive buttons are automatically handled in UIAlertViews and UIActionSheets even with UIAlertControllers are not able to.  The button will still be displayed, and you will get back the correct (buttonIndex == alertController.destructiveButtonIndex).  After iOS > 8.0, you will see the 'Red' color on the button, as expected in new iOS releases.


 

## CREDITS
____
- Andy Newby (Author)
- Some of the internal-mechanics to facilitate the selectionBlocks^{} were modeled after Ryan Maxwell's (UIAlertView-Blocks, UIActionSheet-Blocks,and UIAlertController-Blocks) categories. 'Much Thanks' for his original effort. [github](https://github.com/ryanmaxwell)
 
  
## LICENSE
____
See LICENSE.txt for licensing information for full details.  I'm fine with others using it.