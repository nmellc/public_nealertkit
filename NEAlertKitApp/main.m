//
//  main.m
//  NEAlertKitApp
//
//  Created by Andy Newby on 04/14/15.
//  Copyright © 2016 Newbyman Enterprises, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
