//
//  FirstViewController.m
//  NEAlertKitApp
//
//  Created by Andy Newby on 04/14/15.
//  Copyright © 2016 Newbyman Enterprises, LLC. All rights reserved.
//

#import "FirstViewController.h"
#import <NEAlertKit/NEAlertKit.h>


@interface FirstViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate /*Not Required Only Providing to show Advanced demo options */>
@property (nonatomic, strong) NSMutableArray *alertOptions;
@end


@implementation FirstViewController 



#pragma mark - ViewLifecycle -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.alertOptions = [NSMutableArray new];

    NSDictionary *optionsDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"options" ofType:@"plist"]];
    for(NSDictionary *option in [optionsDict objectForKey:@"options"]) {
        [self.alertOptions addObject:option];
    }
    
}



#pragma mark - UITextFieldDelegate (Advanced Example) -

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    

    NSLog(@"%@ || %@ || %@", string, textField.text, textField);
    

    
    // OPTION - A:  Use If()Else() Tree for explicit logic
    if (textField.tag == NEAlert_Login_TextField_Tag) {
        
        //Login...
        
    }else if (textField.tag == NEAlert_Password_TextField_Tag) {
        
        //Password...
    }
    
    
    // OR....
    
    
    // OPTION - B:  Can use a Switch for explicit logic
    switch (textField.tag) {
        case NEAlert_SecureInput_TextField_Tag:
            
            //Secure Input...
            
            break;
        case NEAlert_PlainInput_TextField_Tag:
            
            //Plain Text...
            
            break;
        case NEAlert_Login_TextField_Tag:
            
            //Login...
            
            break;
        case NEAlert_Password_TextField_Tag:
            
            //Pasword...
            
            break;
        default: {
            
            //Handle your own Textfield(s) in here:
            //...
            
        } break;
            
    }
    
    
    
    return YES;
}


#pragma  mark - UITableViewDataSource -

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    return NSLocalizedString(@"Alert Options", nil);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.alertOptions count];
}



#pragma mark - UITableViewDelegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    //Configure Cell:
    NSDictionary *option = self.alertOptions[indexPath.row];
    cell.textLabel.text = ([option objectForKey:@"title"] ? : @"");

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //Peform Dynamic Selector:
    NSDictionary *option = self.alertOptions[indexPath.row];
    NSString *selectorKey = [option objectForKey:@"selector"];
    SEL selector = NSSelectorFromString(selectorKey);
    if(selector && [self respondsToSelector:selector]) {
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(self, selector);
    }
    
}



#pragma mark - Alerts Examples -

- (IBAction)showAlert1:(id)sender {
    
    [self showOkAlertWithTitle:@"1. Alert Title" message:@"message..." selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
        
        NSLog(@"It Worked!  User selected: %@", buttonTitle);
        
    }];
    
}

- (IBAction)showAlert2:(id)sender {
    
    // NOT Recomended!!  (Just a confirmation button, Hold on Main Thread.  No Assert at this time; however, may be in the future, use with caution.)
    
    [self showOkAlertWithTitle:nil
                       message:nil
                selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                    
                    NSLog(@"User selection made, perform actions...");
                    
                }];
    
}

- (IBAction)showAlert3:(id)sender {
    
    [self showAlertWithTitle:@"3. Alert Title"
                     message:@"message..."
                       style:NEAlertStyle_Default
           cancelButtonTitle:@"Cancel"
      destructiveButtonTitle:@"Destructive"
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
              }];
    
}

- (IBAction)showAlert4:(id)sender {
    
    [self showAlertWithTitle:@"4. Alert Title"
                     message:@"message..."
                       style:NEAlertStyle_Default
           cancelButtonTitle:@"Cancel"
      destructiveButtonTitle:nil
           otherButtonTitles:@[@"option A", @"option B"]
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  if(buttonIndex == alertController.cancelButtonIndex) {
                      //.. Perform Cancel
                      
                  }else if ([buttonTitle isEqualToString:@"option A"]) {
                      
                      //...Perform Option A
                      
                  }else if ([buttonTitle isEqualToString:@"option B"]) {
                      
                      //...Peform Option B
                      
                  }
                  
                  
              }];
    
}

- (IBAction)showAlert5:(id)sender {
    
    [self showAlertWithTitle:@"5. Alert Title"
                     message:@"Secure Text Input"
                       style:NEAlertStyle_SecureTextInput
           cancelButtonTitle:@"Submit"
      destructiveButtonTitle:nil
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  NSLog(@"Text Input: %@", [alertController.textFields.firstObject text]);
                  
                  
              }];
    
}

- (IBAction)showAlert6:(id)sender {
    
    [self showAlertWithTitle:@"6. Alert Title"
                     message:@"Plain Text Input"
                       style:NEAlertStyle_PlainTextInput
           cancelButtonTitle:@"Submit"
      destructiveButtonTitle:nil
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  NSLog(@"Text Input: %@", [alertController.textFields.firstObject text]);
                  
                  
              }];
    
}

- (IBAction)showAlert7:(id)sender {
    
    [self showAlertWithTitle:@"Alert Title7"
                     message:@"Login & Password"
                       style:NEAlertStyle_LoginAndPasswordInput
           cancelButtonTitle:@"Submit"
      destructiveButtonTitle:nil
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  NSLog(@"Login: %@", [alertController.textFields[0] text]);
                  
                  NSLog(@"Password: %@", [alertController.textFields[1] text]);
                  
                  //Use the information...
                  
              }];
    
}

- (IBAction)showAlert8:(id)sender {
    
    [self showAlertWithTitle:@"8. Alert Title"
                     message:@"Lots of buttons..."
                       style:NEAlertStyle_Default
           cancelButtonTitle:@"Cancel"
      destructiveButtonTitle:@"Destructive"
           otherButtonTitles:@[@"a", @"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i"]
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  //Perform login...
                  
              }];
    
}

- (IBAction)showAlert9:(id)sender {
    
    [self showAlertWithTitle:@"9. Alert Title"
                     message:@"Plain Text with Cancel / Submit"
                       style:NEAlertStyle_PlainTextInput
           cancelButtonTitle:@"Cancel"
      destructiveButtonTitle:nil
           otherButtonTitles:@[@"Submit"]
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  NSLog(@"Input: %@", [alertController.textFields[0] text]);
                  
                  if(buttonIndex != alertController.cancelButtonIndex) {
                      
                  }
                  
              }];
    
}

- (IBAction)showAlert10:(id)sender {
    
    UIImage *img = [UIImage imageNamed:@"code_icon"];
    
    [self showImageAlertWithTitle:@"10. Alert Title"
                          message:@"message"
                       sizedImage:img
                cancelButtonTitle:@"Ok"
           destructiveButtonTitle:nil
                otherButtonTitles:nil
                   selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                       
                       NSLog(@"%@", buttonTitle);
                       
                   }];
    
}

- (IBAction)showAlert11:(id)sender {
    
    UIImage *img = [UIImage imageNamed:@"code_icon"];
    
    
    [NEAlertController showImageAlertInViewController:self
                                            withTitle:@"11. Alert Title"
                                              message:@"message"
                                           sizedImage:img
                                    cancelButtonTitle:@"Ok"
                               destructiveButtonTitle:nil
                                    otherButtonTitles:@[@"one", @"two"]
                                       selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                           
                                           NSLog(@"%@", buttonTitle);
                                       }];
    
}

- (IBAction)showAlert12:(id)sender {
    
    /* ASSERT EXAMPLE 
        
       (Review Console)
    
       The Counsol Will Output the following...
     
       *** NEAlertController Assert: Invalid Alert Request.  (No Valid Buttons to dismiss Alert displayed to user.) ***
     
    */
    
    [self showAlertWithTitle:@"12. Alert Title"
                     message:@"Assert Example"
                       style:NEAlertStyle_Default
           cancelButtonTitle:nil
      destructiveButtonTitle:nil
           otherButtonTitles:nil
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  NSLog(@"%@", buttonTitle);
                  
                  
              }];
}

- (IBAction)showAlert13:(id)sender {
    
    //  Extremely Easy to Nest Business Rules or User Input Logic all inline!!
    //  In this example there is a simple 'selections' string keeping track of the selections
    //  However, in a real app, each selection can offer an switch or if()else() logic tree to make the correct logic decisions based on your needs.

    __block NSString *selections = @"";
    
    
    //Alert #1:
    [self showAlertWithTitle:@"13. First Alert 1"
                     message:@"Nested Alerts (Implement logic all inline)"
                       style:NEAlertStyle_Default
           cancelButtonTitle:nil
      destructiveButtonTitle:nil
           otherButtonTitles:@[@"A", @"B", @"C"]
              selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                  
                  selections = [selections stringByAppendingFormat:@"%@",buttonTitle];
                  
                  
                  
                  //Nested Alert #2:
                  [self showAlertWithTitle:@"13. Nested Alert 2"
                                   message:[NSString stringWithFormat:@"You selected: %@", buttonTitle]
                                     style:NEAlertStyle_Default
                         cancelButtonTitle:nil
                    destructiveButtonTitle:nil
                         otherButtonTitles:@[@"D", @"E", @"F"]
                            selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                
                                selections = [selections stringByAppendingFormat:@"-%@",buttonTitle];
                                
                                
                                
                                //Nested Alert #3:
                                [self showAlertWithTitle:@"13. Nested Alert 3"
                                                 message:[NSString stringWithFormat:@"You selected: %@", buttonTitle]
                                                   style:NEAlertStyle_Default
                                       cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                       otherButtonTitles:@[@"H", @"I", @"J"]
                                          selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                              
                                              selections = [selections stringByAppendingFormat:@"-%@",buttonTitle];
                                              
                                              
                                              
                                              //Nested Alert #4:
                                              [self showOkAlertWithTitle:@"13. Nested Alert 4"
                                                                 message:[NSString stringWithFormat:@"Your Nested Selections so far: %@", selections]
                                                          selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                                  
                                                              //Final Log:
                                                              NSLog(@"%@", [NSString stringWithFormat:@"Your Nested Selections: %@", [selections stringByAppendingFormat:@"-%@",buttonTitle]]);
                                                              
                                                          }];
                                               
                                        
                                          }];
                                
                                
                            }];
                  
                  
              }];

    
}

@end
