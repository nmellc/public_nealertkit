//
//  SecondViewController.m
//  NEAlertKitApp
//
//  Created by Andy Newby on 04/14/15.
//  Copyright © 2016 Newbyman Enterprises, LLC. All rights reserved.
//

#import "SecondViewController.h"
#import <NEAlertKit/NEAlertKit.h>


@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation SecondViewController



#pragma mark - ViewLifecycle -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    switch ([[UIDevice currentDevice] userInterfaceIdiom]) {
        case UIUserInterfaceIdiomPad:
            self.infoLabel.text = NSLocalizedString(@"iPad Selections will Present as Popover anchored on Button location", nil);
            break;
        case UIUserInterfaceIdiomPhone:
            self.infoLabel.text = NSLocalizedString(@"iPhone/iPad Selections will Present as normal ActionSheet from anchored from bottom", nil);
            break;
        default:
            self.infoLabel.text = @"";
            break;
    }

}



#pragma mark - ActionSheets -

- (IBAction)showActionSheetFromNavbarButton:(id)sender {
    
    
    [self showActionSheetWithSender:sender
                              title:@"Title"
                            message:@"Message"
                  cancelButtonTitle:@"cancel"
             destructiveButtonTitle:@"destructive"
                  otherButtonTitles:@[@"a",@"b",@"c"]
                     selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                         
                         
                         NSLog(@"Button Selected: %@", buttonTitle);
                         
                         if(buttonIndex == alertController.cancelButtonIndex)
                             NSLog(@"Cancel Index");
                         
                         if(buttonIndex == alertController.destructiveButtonIndex)
                             NSLog(@"Destructive Index");
                         
                         
                     }];
    
    
}

- (IBAction)showActionSheetFromOnScreenButton:(id)sender {
    
    [self showActionSheetWithSender:sender
                              title:@"Title"
                            message:@"Message"
                  cancelButtonTitle:@"cancel"
             destructiveButtonTitle:@"destructive"
                  otherButtonTitles:@[@"a",@"b",@"c"]
                     selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                         
                         
                         NSLog(@"Button Selected: %@", buttonTitle);
                         
                         if(buttonIndex == alertController.cancelButtonIndex)
                             NSLog(@"Cancel Index");
                         
                         if(buttonIndex == alertController.destructiveButtonIndex)
                             NSLog(@"Destructive Index");
                         
                         
                     }];
}

- (IBAction)showActionSheetFromToolbarButton:(id)sender {
    
    [self showActionSheetWithSender:sender
                              title:@"Title"
                            message:@"Message"
                  cancelButtonTitle:@"cancel"
             destructiveButtonTitle:@"destructive"
                  otherButtonTitles:@[@"a",@"b",@"c"]
                     selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                         
                         
                         if(buttonIndex == alertController.cancelButtonIndex) {
                             
                             // Handle Cancel...
                             
                         } else if(buttonIndex == alertController.destructiveButtonIndex) {
                             
                             // Handle Destruction...
                             
                         } else if([buttonTitle isEqualToString:@"a"]) {
                             
                             // Handle A Case...
                             
                         }else if([buttonTitle isEqualToString:@"b"]) {
                             
                             //Handle B Case...
                             
                         }else if([buttonTitle isEqualToString:@"c"]) {
                             
                             //Handle C Case...
                         }
                         
                     }];
}

- (IBAction)showActionSheetUsingNEAlertController:(id)sender {
    
    [NEAlertController showActionSheetInController:self
                                         withTitle:@"Title"
                                           message:@"Message"
                                 cancelButtonTitle:@"cancel"
                            destructiveButtonTitle:@"desctructive"
                                 otherButtonTitles:@[@"a",@"b",@"c"]
                presentationPopoverControllerBlock:^(NEPopoverPresentationController * _Nonnull popover) {
                    popover.sourceView = self.view;
                    popover.sourceRect = [(UIButton *)sender frame];
                }
                                    selectionBlock:^(NEAlertController * _Nonnull alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle) {
                                        
                                        NSLog(@"Button Selected: %@", buttonTitle);
                                        
                                        if(buttonIndex == alertController.cancelButtonIndex)
                                            NSLog(@"Cancel Index");
                                        
                                        if(buttonIndex == alertController.destructiveButtonIndex)
                                            NSLog(@"Destructive Index");
                                        
                                    }];
}



@end
