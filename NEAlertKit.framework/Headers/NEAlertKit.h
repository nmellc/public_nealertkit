//
//  NEAlertKit.h
//  NEAlertKit
//
//  Created by Andy Newby on 04/13/15.
//  Copyright © 2015 Newbyman Enterprises, LLC. All rights reserved.
//
//  Additional Credit:
//  1.  Some of the internal-mechanics were modeled after Ryan Maxwell's (UIAlertView-Blocks, UIActionSheet-Blocks,
//  and UIAlertController-Blocks) categories. ' Much Thanks' for his orignial effort.
//
//  See LICENSE.txt for licensing information
//

#import <UIKit/UIKit.h>

//! Project version number for NEAlertKit.
FOUNDATION_EXPORT double NEAlertKitVersionNumber;

//! Project version string for NEAlertKit.
FOUNDATION_EXPORT const unsigned char NEAlertKitVersionString[];

/**
 *  Import NEAlertKit Framework Classes
 */
#import <NEAlertKit/NEAlertController.h>
