//
//  NEAlertController.h
//  NEAlertController
//
//  Created by Andy Newby on 04/13/15.
//  Copyright © 2015 Newbyman Enterprises, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

/**
 *  Type of Alert Controller (Alert or ActionSheet)
 */
typedef NS_ENUM(NSInteger, NEAlertControllerStyle) {
    /**
     *  Alert-Style presented NEAlertController
     */
    NEAlertControllerStyle_Alert = 0,
    /**
     *  ActionSheet-Style presented NEAlertController
     */
    NEAlertControllerStyle_ActionSheet,
};

/**
 *  Type of NEAlertControllerStyle_Alert Preconfigured configurations to support { Deafult, SecureText, PlainText, or Login/Password }  (Extended to support all iOS versions to support both UIAlertView and UIAlertControllers Automatically)
 */
typedef NS_ENUM(NSInteger, NEAlertStyle) {
    /**
     *  An alert that presents a standard title, message; original UIAlertView-styled alert.  There are no UITextFields, nor delegation methods.
     */
    NEAlertStyle_Default = 0,
    /**
     *  An alert that presents a title, message, and single secure text input.
     *  Delegation is honored if you add the <UITextFieldDelegate> protocol to your ViewControllers interface and Utilize the UITextField methods.
     *  Note:  If you need a test case for each UITextField within the delegate method, use  the following logic  if(textField.tag == [NEAlertController firstTextFieldTag]) {..}
     *  SecureTextInput(textField.tag)      = [NEAlertController firstTextFieldTag]
     */
    NEAlertStyle_SecureTextInput,
    /**
     *  An alert that presents a title, message, and single text input.
     *  Delegation is honored if you add the <UITextFieldDelegate> protocol to your ViewControllers interface and Utilize the UITextField methods.
     *  Note:  If you need a test case for each UITextField within the delegate method, use  the following logic  if(textField.tag == [NEAlertController firstTextFieldTag]) {..}
     *  PainTextInput(textField.tag)      = [NEAlertController firstTextFieldTag]
     */
    NEAlertStyle_PlainTextInput,
    /**
     *  An alert that presents a title, message, and both login & password text fields.
     *  Delegation is honored if you add the <UITextFieldDelegate> protocol to your ViewControllers interface and Utilize the UITextField methods.
     *  Note:  If you need a test case for each UITextField within the delegate method, use  the following logic  if(textField.tag == [NEAlertController firstTextFieldTag]) {..} or [NEAlertController secondTextFieldTag] for latter.
     *  LoginTextInput(textField.tag)       = [NEAlertController firstTextFieldTag]
     *  PasswordTextInput(textField.tag)    = [NEAlertController secondTextFieldTag]
     */
    NEAlertStyle_LoginAndPasswordInput
};

/**
 *  (!!! ONLY IF NEEDED !!!)  Unique UITextField Tags to support <UITextFieldDelegate> differentiation operations when working with more than one UITextField within an UIViewController or within the NEAlertController.
 *  NOTE:  This is not needed for access the textField.text value.  That is provided within the selectedBlock() by accessing 'alertController.textFields[0].text'
 */
typedef NS_ENUM(NSInteger, NEAlertTextFieldTag) {
    /**
     *  Unique Tag for NEAlertStyle_SecureTextInput : TextField (1)->{Secure Text} to support delegation access for comparision logic against other UITextFields
     */
    NEAlert_SecureInput_TextField_Tag = -88801,
    /**
     *  Unique Tag for NEAlertStyle_PlainTextInput : TextField (1)->{Plain Text} to support delegation access for comparision logic against other UITextFields
     */
    NEAlert_PlainInput_TextField_Tag = -88802,
    /**
     *  Unique Tag for NEAlertStyle_LoginAndPasswordInput : TextField[0]->{Login} to support delegation access for comparision logic against other UITextFields
     */
    NEAlert_Login_TextField_Tag = -88803,
    /**
     *  Unique Tag for NEAlertStyle_LoginAndPasswordInput : TextField[1]->{Password} to support delegation access for comparision logic against other UITextFields
     */
    NEAlert_Password_TextField_Tag = -88804
};


@class NEAlertController;
@class NEPopoverPresentationController;


/**
 *  NEAlertSelectionBlock provides an inline execution completion block that provides access to the alertController, buttonIndex, and selected buttonTitle
 *
 *  @author                Andy Newby
 *  @see NEAlertController documentation for additional use cases.
 *
 *  @param alertController NEAlertController instance
 *  @param buttonIndex     Selected button index
 *  @param buttonTitle     Selected button title
 */
typedef void (^NEAlertSelectionBlock) (NEAlertController * alertController, NSInteger buttonIndex, NSString * _Nullable buttonTitle);


/**
 *  NEPopoverPresentationBlock provides access to the Presented Popover Controller to adjust size, constraints, and correct positioning for use on iPad
 *
 *  @author                Andy Newby
 *  @param popover NEPopoverPresentationController instance to controll and access the UIPopoverPresentationController delegation.
 *  @code
 *  presentationPopoverControllerBlock:^(NEPopoverPresentationController * _Nonnull popover) {
 *      if(sender) {
 *          popover.sourceView = self.view;
 *          popover.sourceRect = sender.frame;
 *      }
 *  }
 *  @endcode
 *  @see UIPopoverPresentationController (iPad)
 *  @warning You should fully understand how to leverage UIPopoverPresentationController correctly, OR use the UIViewController category method [self showActionSheet:::]' instead which handles this logic for you.
 *
 *  @param .barButtonItem The bar button item on which to anchor the popover.
 *  @param .sourceView The view containing the anchor rectangle for the popover.
 *  @param .sourceRect The rectangle in the specified view in which to anchor the popover.
 *
 */
typedef void (^NEPopoverPresentationControllerBlock) (NEPopoverPresentationController * __nonnull popover);


/**
 *  Powerful AlertController Class for iOS (5.0+) That utilizes the correct UIAlertView, UIActionSheet, or UIAlertController to offer a single class of implementations that will correctly target your iOS versio and offer inline selectedBlock^{} execution.
 *
 *  @author                Andy Newby
 *  @see NEAlertSelectionBlock documentation for access information.
 *  @attention             When implementing either the UIViewController category extension or NEAlertController; the inline selectionBlock^{} can be leveraged as shown below:
 *
 *  @code {cancelButtonIndex Logic}
 *  if(buttonIndex == alertContrller.cancelButtonIndex) {
 *       Perform Cancel code here ...
 *  }
 *  @endcode
 *  @code {destructiveButtonIndex Logic}
 *  if(buttonIndex == alertContrller.destructiveButtonIndex) {
 *       Perform Destructive code here...
 *  }
 *  @endcode
 *  @code {buttonTitle}
 *  NSLog(@"%@", buttonTitle);
 *  @endcode
 *  @code {buttonTitle Logic}
 *  if ([buttonTitle isEqualToString:@"Title A"]) {
 *      Perform Option A code here...
 *  }else if ([buttonTitle isEqualToString:@"Title B"]) {
 *      Peform Option B code here...
 *  }
 *  @endcode
 *  @code {Typical Logic}
 *  if(buttonIndex == alertController.cancelButtonIndex) {
 *      Perform Cancel code here ...
 *  else if(buttonIndex == alertController.cancelButtonIndex) {
 *      Perform Destructive code here...
 *  }else if ([buttonTitle isEqualToString:@"Title A"]) {
 *      Perform Option A code here...
 *  }else if ([buttonTitle isEqualToString:@"Title B"]) {
 *      Peform Option B code here...
 *  }
 *  @endcode
 *  @code {textFields}.NEAlertStyle_LoginAndPasswordInput
 *
 *  NSLog(@"Login: %@", [alertController.textFields[0] text]);
 *
 *  NSLog(@"Password: %@", [alertController.textFields[1] text]);
 *  @endcode
 *  @code {textFields}.NEAlertStyle_PlainTextInput
 *
 *  NSLog(@"Text Input was: %@", [alertController.textFields.firstObject text]);
 *  @endcode
 *  @param .cancelButtonIndex (readonly) Index of the Cancel Button that can be used for comparision against the 'buttonIndex' within selectedBlock(buttonIndex)
 *  @param .destructiveButtonIndex (readonly) Index of the Destructive Button that can be used for comparision against the 'buttonIndex' within selectedBlock(buttonIndex)
 *  @param .textFields (readonly) Access to the Array of UITextFields.
 */
@interface NEAlertController : NSObject

@property (readonly, nonatomic) NSInteger cancelButtonIndex;
@property (readonly, nonatomic) NSInteger destructiveButtonIndex;
@property (readonly, nonatomic) NSArray * _Nullable textFields;


/**
 *  Convience Method to implement the correct UIAlertView/UIAlertController necessary to present UIAlert-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @attention Designed to address the 90% usage scenerio to present the user with a simple "Ok" Alert and offer an inline code execution dismiss Block^{} to execute after user dismiss alert.
 *
 *  @author               Andy Newby
 *  @param viewController Current ViewController
 *  @param title          Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message        Message of alert to present to user.
 *  @param selectionBlock Optional inline Completion Block^{} to execute after selection.
 */
+ (void)showOkAlertInViewController:(UIViewController *)viewController
                          withTitle:(NSString * _Nullable)title
                            message:(NSString * _Nullable)message
                     selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;

/**
 *  Convience Method to implement the correct UIAlertView/UIAlertController necessary to present UIAlert-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @author                 Andy Newby
 *  @param viewController   Current ViewController
 *  @param title            Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message          Message of alert to present to user.
 *  @param style            Style of Alert that should be presented, view Enum 'NEAlertStyle'
 *  @param cancelTitle      Title of the Cancel Button
 *  @param destructiveTitle Title of Destructive Button (supported in Red > iOS 8.0, supported as additional button < iOS 8.0)
 *  @param otherTitles      Optional other Button titles, each title will create another actionable button on alert for user.
 *  @param selectionBlock   Optional inline Completion Block^{} to execute after selection.
 */
+ (void)showAlertInViewController:(UIViewController *)viewController
                        withTitle:(NSString * _Nullable)title
                          message:(NSString * _Nullable)message
                            style:(NEAlertStyle)style
                cancelButtonTitle:(NSString * _Nullable)cancelTitle
           destructiveButtonTitle:(NSString * _Nullable)destructiveTitle
                otherButtonTitles:(NSArray <NSString *> * _Nullable)otherTitles
                   selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;

/**
 *  Convience Method to implement an Imaged Alert of the correct UIAlertView/UIAlertController necessary to present UIAlert-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @author                 Andy Newby
 *  @attention              This method should be considered a novality method (nice-to-have).  It currently works on iPhone and iPad; however, it is not guaranteed to work on all devices for all time.  It does fill a unique purpose and void in the out-of-the box implementation.
 *  @param viewController   Current ViewController
 *  @param title            Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message          Message of alert to present to user.
 *  @param sizedImage       UIImage, will get resized to meet internal Demands if larger than 80 x 80 pts
 *  @param cancelTitle      Title of the Cancel Button
 *  @param destructiveTitle Destructive Button Title (supported in Red > iOS 8.0, supported as additional button < iOS 8.0)
 *  @param otherTitles      Optional other Button titles, each title will create another actionable button on alert for user.
 *  @param selectionBlock   Optional inline Completion Block^{} to execute after selection.
 */
+ (void)showImageAlertInViewController:(UIViewController *)viewController
                             withTitle:(NSString * _Nullable)title
                               message:(NSString * _Nullable)message
                            sizedImage:(UIImage * _Nullable)sizedImage
                     cancelButtonTitle:(NSString * _Nullable)cancelTitle
                destructiveButtonTitle:(NSString * _Nullable)destructiveTitle
                     otherButtonTitles:(NSArray <NSString *> * _Nullable)otherTitles
                        selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;

/**
 *  Convience Method to implement the correct UIActionSheet/UIAlertController necessary to present UIActionSheet-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @author                 Andy Newby
 *  @see NEPopoverPresentationControllerBlock for additional details if implmeneting manually on iPad.
 *
 *  @param viewController   Presenting UIViewController (typically self)
 *  @param title            Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message          Message of alert to present to user. (Automatically extended to supported messages < iOS8; they will present below the Title)
 *  @param cancelTitle      Title of the Cancel Button (supported on iPhone/iPod as ActionSheet button, Not shonw on iPad, as the button is implemented by touching anywhere outside the UIPopoverViewcontroller) However, Block^{} still fires for you with correct cancelButtonIndex.
 *  @param destructiveTitle Title of Destructive Button (supported in Red > iOS 8.0, supported as additional button < iOS 8.0)
 *  @param otherTitles      Optional other Button titles, each title will create another actionable button on ActionSheet for user.
 *  @param popoverBlock     NEPopoverPresentationControllerBlock exection access to configure your UIpopoverViewController (Not needed if using UIViewController category methods; these operations are handled for you)
 *  @param selectionBlock   Optional inline Completion Block^{} to execute after selection.
 */
+ (void)showActionSheetInController:(UIViewController *)viewController
                          withTitle:(NSString * _Nullable)title
                            message:(NSString * _Nullable)message
                  cancelButtonTitle:(NSString * _Nullable)cancelTitle
             destructiveButtonTitle:(NSString * _Nullable)destructiveTitle
                  otherButtonTitles:(NSArray <NSString *> * _Nullable)otherTitles
 presentationPopoverControllerBlock:(NEPopoverPresentationControllerBlock _Nullable)popoverBlock
                     selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;


@end

NS_ASSUME_NONNULL_END

NS_ASSUME_NONNULL_BEGIN

@interface NEPopoverPresentationController : NSObject

@property(nonatomic, strong) UIBarButtonItem *barButtonItem;
@property(nonatomic, strong) UIView *sourceView;
@property(nonatomic, assign) CGRect sourceRect;

@end

NS_ASSUME_NONNULL_END

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (NEAlertController) <UITextFieldDelegate>

/**
 *  Convience Method to implement simple "Ok" alert of the correct UIAlertView/UIAlertController necessary to present UIAlert-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @author               Andy Newby
 *  @param title          Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message        Message of alert to present to user.
 *  @param selectionBlock Optional inline Completion Block^{} to execute after selection.
 */
- (void)showOkAlertWithTitle:(NSString * _Nullable)title
                     message:(NSString * _Nullable)message
              selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;

/**
 *  Convience Method to implement a dynamic Alert based on the NEAlertStyle of the correct UIAlertView/UIAlertController necessary to present UIAlert-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @author                 Andy Newby
 *  @param title            Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message          Message of alert to present to user.
 *  @param style            Style of Alert that should be presented, view Enum 'NEAlertStyle'
 *  @param cancelTitle      Title of the Cancel Button
 *  @param destructiveTitle Destructive Button Title (supported in Red > iOS 8.0, supported as additional button < iOS 8.0)
 *  @param otherTitles      Optional other Button titles, each title will create another actionable button on alert for user.
 *  @param selectionBlock   Optional inline Completion Block^{} to execute after selection.
 */
- (void)showAlertWithTitle:(NSString * _Nullable)title
                   message:(NSString * _Nullable)message
                     style:(NEAlertStyle)style
         cancelButtonTitle:(NSString * _Nullable)cancelTitle
    destructiveButtonTitle:(NSString * _Nullable)destructiveTitle
         otherButtonTitles:(NSArray <NSString *> * _Nullable)otherTitles
            selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;

/**
 *  Convience Method to implement an Imaged Alert of the correct UIAlertView/UIAlertController necessary to present UIAlert-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @author                 Andy Newby
 *  @attention              This method should be considered a novality method (nice-to-have).  It currently works on iPhone and iPad; however, it is not guaranteed to work on all devices for all time.  It does fill a unique purpose and void in the out-of-the box implementation.
 *  @param title            Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message          Message of alert to present to user.
 *  @param sizedImage       UIImage, will get resized to meet internal Demands if larger than 80 x 80 pts
 *  @param cancelTitle      Title of the Cancel Button
 *  @param destructiveTitle Destructive Button Title (supported in Red > iOS 8.0, supported as additional button < iOS 8.0)
 *  @param otherTitles      Optional other Button titles, each title will create another actionable button on alert for user.
 *  @param selectionBlock   Optional inline Completion Block^{} to execute after selection.
 */
- (void)showImageAlertWithTitle:(NSString * _Nullable)title
                        message:(NSString * _Nullable)message
                     sizedImage:(UIImage * _Nullable)sizedImage
              cancelButtonTitle:(NSString * _Nullable)cancelTitle
         destructiveButtonTitle:(NSString * _Nullable)destructiveTitle
              otherButtonTitles:(NSArray <NSString *> * _Nullable)otherTitles
                 selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;


/**
 *  Convience Method to implement the correct UIActionSheet/UIAlertController necessary to present UIActionSheet-sytled alert based on iOS version and provide optional dismiss Block^{} to execute inline code with no delegation required.
 *
 *  @author                 Andy Newby
 *  @param sender           Pass a (UIButton or UIBarButtonItem or UIView subclass) that represents the 'Anchor' Object of the ActionSheet for (iPad).
 *  @param title            Title of alert to present to user.  (Nil Permitted, will not show title)
 *  @param message          Message of alert to present to user. (Automatically extended to supported messages < iOS8; they will present below the Title)
 *  @param cancelTitle      Title of the Cancel Button (supported on iPhone/iPod as ActionSheet button, Not shonw on iPad, as the button is implemented by touching anywhere outside the UIPopoverViewcontroller) However, Block^{} still fires for you with correct cancelButtonIndex.
 *  @param destructiveTitle Title of Destructive Button (supported in Red > iOS 8.0, supported as additional button < iOS 8.0)
 *  @param otherTitles      Optional other Button titles; each title will create another actionable button on ActionSheet for user.
 *  @param selectionBlock   Optional inline Completion Block^{} to execute after selection.
 */
- (void)showActionSheetWithSender:(id)sender
                            title:(NSString * _Nullable)title
                          message:(NSString * _Nullable)message
                cancelButtonTitle:(NSString * _Nullable)cancelTitle
           destructiveButtonTitle:(NSString * _Nullable)destructiveTitle
                otherButtonTitles:(NSArray <NSString *> * _Nullable)otherTitles
                   selectionBlock:(NEAlertSelectionBlock _Nullable)selectionBlock;

@end

NS_ASSUME_NONNULL_END


